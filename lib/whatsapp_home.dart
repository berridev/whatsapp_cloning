import 'package:flutter/material.dart';
import 'package:whatsapp/pages/camera.dart';
import 'package:whatsapp/pages/chat.dart';
import 'package:whatsapp/pages/panggilan.dart';
import 'package:whatsapp/pages/status.dart';

class WhatsAppHome extends StatefulWidget {
  @override
  _WhatsAppHomeState createState() => _WhatsAppHomeState();
}

class _WhatsAppHomeState extends State<WhatsAppHome> 
  with SingleTickerProviderStateMixin {
    TabController _tabController;
  @override
  void initState(){
    super.initState();
    _tabController = TabController(vsync: this, initialIndex: 1,  length: 4 );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("WhatsaApp Clone"),
        elevation: 0.7,
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.camera_alt)),
            Tab(text: "CHAT"),
            Tab(text: "STATUS"),
            Tab(text: "PANGGILAN"),
          ],
        ),
        actions: <Widget>[
          Icon(Icons.search),
          Padding(padding: const EdgeInsets.symmetric(horizontal: 5.0)),
          Icon(Icons.more_vert)
        ],
      ),
      body: TabBarView(controller: _tabController,
      children: <Widget>[
        LayarCamera(),
        LayarChat(),
        LayarStatus(),
        LayarPanggilan(),
      ],),
      floatingActionButton: FloatingActionButton(backgroundColor: Theme.of(context).accentColor,
      child: Icon(Icons.message, color: Colors.white),
      onPressed: () => print("Buka Pesan"),),
    );
  }
}
  