class ChatModel {
  final String nama;
  final String pesan;
  final String waktu;
  final String urlAvatar;

  ChatModel({this.nama, this.pesan, this.waktu, this.urlAvatar});
}

List<ChatModel> contohData = [
  ChatModel(
      nama: "Berri Primaputra",
      pesan: "hai.. ini adalah sample aplikasi whatsapp menggunakan flutter",
      waktu: "14.20",
      urlAvatar:
          "https://gitlab.com/berridev/whatsapp_cloning/blob/master/lib/assets/images/avatar1.png"),
  ChatModel(
      nama: "Bapak Hasan",
      pesan: "hai.. ini adalah sample aplikasi whatsapp menggunakan flutter",
      waktu: "14.20",
      urlAvatar:
          "https://gitlab.com/berridev/whatsapp_cloning/blob/master/lib/assets/images/avatar2.png"),
  ChatModel(
      nama: "Ibu Maymunah",
      pesan: "hai.. ini adalah sample aplikasi whatsapp menggunakan flutter",
      waktu: "14.20",
      urlAvatar:
          "https://gitlab.com/berridev/whatsapp_cloning/blob/master/lib/assets/images/avatar3.png"),
  ChatModel(
      nama: "Tok Isnen",
      pesan: "hai.. ini adalah sample aplikasi whatsapp menggunakan flutter",
      waktu: "14.20",
      urlAvatar:
          "https://gitlab.com/berridev/whatsapp_cloning/blob/master/lib/assets/images/avatar4.png"),
  ChatModel(
      nama: "Bu Aya",
      pesan: "hai.. ini adalah sample aplikasi whatsapp menggunakan flutter",
      waktu: "14.20",
      urlAvatar:
          "https://gitlab.com/berridev/whatsapp_cloning/blob/master/lib/assets/images/avatar5.png"),
  ChatModel(
      nama: "Pak Aya",
      pesan: "hai.. ini adalah sample aplikasi whatsapp menggunakan flutter",
      waktu: "14.20",
      urlAvatar:
          "https://gitlab.com/berridev/whatsapp_cloning/blob/master/lib/assets/images/avatar6.png"),

];
